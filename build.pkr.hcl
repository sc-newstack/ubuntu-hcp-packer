build {
   hcp_packer_registry {
    bucket_name = "burkey-hcp-packer"
    description = <<EOT
Packer Image in HCP registry.
    EOT
    labels = {
      "image_version" = "1.1",
      "environment"         = "production",
      "approved" = "true",
      "owner" = "burkey",
      "cloud" = "ready"
    }
  }
  sources = [
		#"vsphere-iso.ubuntu-1804",
		"amazon-ebs.base"
  ]

  provisioner "ansible" {
    playbook_file = "./playbook.yaml"
    user = "ubuntu"
    extra_arguments = [
			"--extra-vars",
			"vault_addr=${var.vault_addr} ansible_user_password=${var.ansible_user_password}"
		]
  }
}
